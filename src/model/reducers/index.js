import reducerBucket from './bucket';
import reducerProduct from './product';
import { sanitize } from 'dompurify';
import Parser from 'html-react-parser';

const initialState = 
  {
    product: [],
    bucket: []
};

const productAddToBucket = (idx, bucket, product) => {
    const newProduct = [
      ...product.slice(0,idx),
      ...product.slice(idx+1)
    ];
    
    if (product[idx].active) {
      if (newProduct[idx]) {
        newProduct[idx].active = true;
      } else if (newProduct[idx - 1]) {
        newProduct[idx - 1].active = true;
      }
    }

    const bucketProduct = { ...product[idx], active: false };

    return {
      product: newProduct,
      bucket: [...bucket, bucketProduct]
  }
}

const bucketRemovePicked = (product, bucket) => {
    const newProducts = [...product];
    const newBucket = [];
    bucket.forEach(item => {
        if (!item.active) {
            newBucket.push(item);
        } else {
            const newItem = { ...item };
            newItem.active = false;
            newProducts.push(newItem);
        }
    });
    if (product.length === 0) {
        newProducts[0].active = true;
    }
    return {
      product: newProducts,
      bucket: newBucket
    }
}

const productVerifier = (product) => {
    if (product.length > 0) {
        if (product.findIndex(item => item.active) === -1) {
          product[0].active = true;
        }
    }

    return product.map(item => {
        return {
            ...item,
            name: Parser(sanitize(item.name)),
            description: Parser(sanitize(item.description))
        }
    })
}

const reducer = (state = initialState, action) => {
    const { product, bucket } = state;

    switch (action.type) {
      case 'FETCH_PRODUCTS_REQUEST':
          return {
            ...state,
            products: [],
            loading: true,
            error: null
          };
    
        case 'FETCH_PRODUCTS_SUCCESS':
          return {
            ...state,
            product: productVerifier(action.payload),
            loading: false,
            error: null
          };
    
        case 'FETCH_PRODUCTS_FAILURE':
          return {
            ...state,
            products: [],
            loading: false,
            error: action.payload
          };

      case 'PRODUCT_MOVE_DOWN_ACTIVE':
      case 'PRODUCT_MOVE_UP_ACTIVE':
          return {
            ...state,
            product: reducerProduct(product, action)
          };

      case 'PRODUCT_ADD_ACTIVE_TO_BUCKET':
          const activeIndex = product.findIndex(item => item.active);
          return productAddToBucket(activeIndex, bucket, product);

      case 'PRODUCT_ADD_TO_BUCKET':
          const id = action.payload;
          const idx = product.findIndex(item => item.id === id);
          return productAddToBucket(idx, bucket, product);
          

      case 'BUCKET_REMOVE_PICKED_PRODUCTS':
          return bucketRemovePicked(product, bucket);

      case 'BUCKET_PICK_NOTHING':
      case 'BUCKET_PICK_PRODUCT':
      case 'BUCKET_PICK_ALL':
            return {
              ...state,
              bucket: reducerBucket(bucket, action)
            }

      default:
        return state;
    }
  };
  
  export default reducer;