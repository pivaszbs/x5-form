const productMoveActive = (product, direction) => {
  const idxActive = product.findIndex(item => item.active);
  
  //direction === -1 UP
  //direction === 1 DOWN
  if (
    (idxActive === product.length - 1 && direction === 1) ||
    (idxActive === 0 && direction === -1)) {
      return product;
  }

  const notActive = { ...product[idxActive], active: false };
  const newActive = { ...product[idxActive + direction], active: true};

  if (direction === 1) {
    return [
      ...product.slice(0, idxActive),
      notActive,
      newActive,
      ...product.slice(idxActive + 2)
    ];
  }

  return [
    ...product.slice(0, idxActive - 1),
    newActive,
    notActive,
    ...product.slice(idxActive + 1)
  ];
}

const reducerProduct = (product, action) => {
    switch (action.type) {
      case 'PRODUCT_MOVE_DOWN_ACTIVE':
          return productMoveActive(product, 1)
      
      case 'PRODUCT_MOVE_UP_ACTIVE':
          return productMoveActive(product, -1)
      
      default: return;
    }
  };
  
  export default reducerProduct;