const bucketReducer = (bucket, action) => {
    switch (action.type) {
        case 'BUCKET_PICK_NOTHING':
            return bucket.map(item => {
              return { ...item, active: false }
          });

        case 'BUCKET_PICK_PRODUCT':
          const id = action.payload;
          const idx = bucket.findIndex(item => item.id === id);
          const newItem = { ...bucket[idx], active: !bucket[idx].active }

          return [
              ...bucket.slice(0, idx),
              newItem,
              ...bucket.slice(idx+1)
          ];
        
        case 'BUCKET_PICK_ALL':
          return bucket.map(item => {
              return { ...item, active: true }
          });
        
        default: return;
    }
  };
  
export default bucketReducer;