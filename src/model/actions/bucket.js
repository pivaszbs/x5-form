const bucketRemovePickedProducts = () => {
    return {
        type: 'BUCKET_REMOVE_PICKED_PRODUCTS'
    }
}

const bucketPickNothing = () => {
    return {
        type: 'BUCKET_PICK_NOTHING'
    }
}

const bucketPickProduct = (id) => {
    return {
        type: 'BUCKET_PICK_PRODUCT',
        payload: id
    }
}

const bucketPickAll = () => {
    return {
        type: 'BUCKET_PICK_ALL'
    }
}

export {
    bucketPickAll,
    bucketPickNothing,
    bucketPickProduct,
    bucketRemovePickedProducts
}