const productActiveMoveDown = () => {
    return {
        type: 'PRODUCT_MOVE_DOWN_ACTIVE'
    }
}

const productActiveMoveUp = () => {
    return {
        type: 'PRODUCT_MOVE_UP_ACTIVE'
    }
}

const productAddActiveToBucket = () => {
    return {
        type: 'PRODUCT_ADD_ACTIVE_TO_BUCKET'
    }
};

const productAddToBucket = (id) => {
    return {
        type: 'PRODUCT_ADD_TO_BUCKET',
        payload: id
    }
};

const productsRequested = () => {
    return {
      type: 'FETCH_PRODUCTS_REQUEST'
    }
  };
  
  const productsLoaded = (newProducts) => {
    return {
      type: 'FETCH_PRODUCTS_SUCCESS',
      payload: newProducts
    };
  };
  
  const productsError = (error) => {
    return {
      type: 'FETCH_PRODUCTS_FAILURE',
      payload: error
    };
  };

  const fetchProducts = (formService, dispatch) => () => {
    dispatch(productsRequested());
    formService.getProducts()
      .then((data) => dispatch(productsLoaded(data)))
      .catch((err) => dispatch(productsError(err)));
  };

export {
    productActiveMoveDown,
    productActiveMoveUp,
    productAddActiveToBucket,
    productAddToBucket,
    fetchProducts
}