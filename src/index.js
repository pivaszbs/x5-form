import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './components/app/App';
import ErrorBoundry from './components/error-boundary';
import FormService from './services/form-service';
import { FormServiceProvider } from './components/form-service-context';

import './styles/global-styles.sass';

import store from './model';

const formService = new FormService();

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundry>
      <FormServiceProvider value={formService}>
        <App />
      </FormServiceProvider>
    </ErrorBoundry>
  </Provider>,
  document.getElementById('root')
);