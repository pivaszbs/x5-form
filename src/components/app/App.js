import React from 'react';
import './app.sass';
import Bucket from '../bucket';
import Products from '../products';
import { Row } from '../ui';
import ControlButtons from '../control-buttons/control-buttons';

const App = (props) => {
  return (
    <div className='app'>
        <Row className='app__content'>
          <Products />
          <Bucket />
        </Row>
        <ControlButtons />
    </div>
  );
}

export default App;