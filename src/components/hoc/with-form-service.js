import React from 'react';
import { FormServiceConsumer } from '../form-service-context';

const withFormService = () => (Wrapped) => {

  return (props) => {
    return (
      <FormServiceConsumer>
        {
          (formService) => {
            return (<Wrapped {...props}
                     formService={formService}/>);
          }
        }
      </FormServiceConsumer>
    );
  }
};

export default withFormService;