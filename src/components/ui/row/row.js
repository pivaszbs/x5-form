import React from 'react';
import './row.sass';
import clsx from 'clsx';

function Row({ children, className, ...others }) {
    return (
        <div className={clsx('row', className)} {...others}>
            {children}
        </div>
    )
}

export default Row
