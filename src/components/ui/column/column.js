import clsx from 'clsx';
import React from 'react';
import './column.sass';

function Column({ children, className, ...others} ) {
    return (
        <div className={clsx('column', className)} {...others}>
            {children}
        </div>
    )
}

export default Column