import React from 'react'
import './checkbox.sass';
import clsx from 'clsx';

function Checkbox({ active, className }) {
    return (
        <div className={clsx('checkbox', className)}>
            <i className={clsx(
                    'material-icons',
                    'checkbox__check',
                    active && 'checkbox__check_active'
                )}
            >
                done
            </i>
        </div>
    )
}

export default Checkbox
