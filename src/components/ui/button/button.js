import React from 'react'
import clsx from 'clsx';
import Text from '../text';
import './button.sass';

const variantMapping = {
    primary: 'button_primary',
    secondary: 'button_secondary'
};

const Button = (props) => {
    let {
        children,
        variant = 'primary',
        icon,
        className,
        active,
        textVariant = 'body',
        ...other
    } = props;

    const inner = () => {
        const text = (
            <Text variant={textVariant} className='button__text'>{children}</Text>
        );
        if (icon) {
            return icon;
        }

        return text;
    };

    return (
        <button
            disabled={active === false}
            className={clsx(
                        active && 'button_active',
                        'button',
                        variantMapping[variant],
                        className)}
            {...other}
        >
            {inner()}
        </button>
    )
};

export default Button
