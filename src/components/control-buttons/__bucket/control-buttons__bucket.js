import React from 'react'
import { Row, Button } from '../../ui';
import { bucketPickAll, bucketRemovePickedProducts, bucketPickNothing } from '../../../model/actions';
import { connect } from 'react-redux';

function ControlButtonsBucket({
    pickAll,
    removePicked,
    pickNothing,
    pickAllActive,
    pickNothingActive,
    removePickedActive
}) {
    return (
        <Row className='control-buttons__bucket'>
            <Button
                active={pickAllActive}
                onClick={pickAll}
                className='control-buttons__button'>
                Выделить все
            </Button>
            <Button
                active={pickNothingActive}
                onClick={pickNothing}
                className='control-buttons__button'>
                Выбрать ничего
            </Button>
            <Button
                active={removePickedActive}
                onClick={removePicked}
                className='control-buttons__button'>
                Удалить выделенные
            </Button>
        </Row>
    )
}

const mapStateToProps = ({ bucket }) => {
    let pickAllActive = false;
    let removePickedActive = false;
    let pickNothingActive = false;
    if (bucket.length > 0) {
        if (bucket.findIndex(item => item.active) === -1) {
            pickAllActive = true;
        } else if (bucket.findIndex(item => !item.active) === -1) {
            removePickedActive = true;
            pickNothingActive = true;
        } else {
            pickAllActive = true;
            removePickedActive = true;
            pickNothingActive = true;
        }
    }

    return {
        pickAllActive,
        removePickedActive,
        pickNothingActive
    }
};

const mapDispatchToProps = {
    pickAll: bucketPickAll,
    removePicked: bucketRemovePickedProducts,
    pickNothing: bucketPickNothing
}

export default connect(mapStateToProps, mapDispatchToProps)(ControlButtonsBucket)