import React from 'react'
import { Row } from '../ui';
import './control-buttons.sass';
import ControlButtonsProducts from './__products/control-buttons__products';
import ControlButtonsBucket from './__bucket/control-buttons__bucket';

function ControlButtons() {
    return (
        <Row className='control-buttons'>
            <ControlButtonsProducts />
            <ControlButtonsBucket />
        </Row>
    )
}

export default ControlButtons
