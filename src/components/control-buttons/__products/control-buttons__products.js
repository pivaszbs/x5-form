import React from 'react'
import { Button, Row } from '../../ui';
import { connect } from 'react-redux';
import { productActiveMoveDown, productActiveMoveUp, productAddActiveToBucket } from '../../../model/actions';

const ControlButtonUp = (props) => {
    const icon = (
        <i className='material-icons'>
            expand_less
        </i>
    );
    
    return <Button className='control-buttons__button' icon={icon} {...props}/>
};

const ControlButtonDown = (props) => {
    const icon = (
        <i className='material-icons'>
            expand_more
        </i>
    );
    
    return <Button className='control-buttons__button' icon={icon} {...props} />
};

function ControlButtonsProducts({ onUp, onDown, onAdd, upActive, downActive, addActive }) {
    return (
        <Row className='control-buttons__products'>
            <ControlButtonUp active={upActive} onClick={ onUp } />
            <ControlButtonDown active={downActive} onClick={ onDown } />
            <Button active={addActive} onClick={ onAdd } >Добавить</Button>
        </Row>
    )
}

const mapStateToProps = ({ product }) => {
    let upActive = false;
    let downActive = false;
    let addActive = false;
    if (product.length > 0) {
        addActive = true;
        if (product.length > 1) {
            if (product[0].active) {
                downActive = true;
            } else if (product[product.length - 1].active) {
                upActive = true;
            } else {
                downActive = true;
                upActive = true;
            }
        }
    }

    return {
        upActive,
        downActive,
        addActive
    }
}

const mapDispatchToProps = {
    onUp: productActiveMoveUp,
    onDown: productActiveMoveDown,
    onAdd: productAddActiveToBucket
}

export default connect(mapStateToProps, mapDispatchToProps)(ControlButtonsProducts)