import React from 'react';
import './bucket.sass';
import { Column } from '../ui';
import { connect } from 'react-redux';
import BucketRow from './__row/bucket__row';

function Bucket({ items = [] }) {
    const bucketItems = items.map(item => (
        <BucketRow item={item} />
    ));

    return (
        <Column className='bucket'>
            {bucketItems}
        </Column>
    )
}

const mapStateToProps = ({ bucket }) => {
    return {
        items: bucket
    }
}

export default connect(mapStateToProps)(Bucket)