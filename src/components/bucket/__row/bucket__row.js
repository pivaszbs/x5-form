import React from 'react'
import { Row, Column, Text, Checkbox } from '../../ui';
import { bucketPickProduct } from '../../../model/actions';
import { connect } from 'react-redux';

function BucketRow({ item, pick }) {
    const { active, artNo, name, description, id } = item;

    return (
        <Row onClick={() => pick(id)} className='bucket__row'>
            <Column className='bucket__text'>
                <Text className='bucket__article' variant='body'>Арт: {artNo}</Text>
                <Text className='bucket__name' variant='h2'>{name}</Text>
                <Text className='bucket__description' variant='h3'>Арт: {description}</Text>
            </Column> 
            <Checkbox className='bucket__checkbox' active={active} />
        </Row>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        pick: (id) => dispatch(bucketPickProduct(id))
    }
}

export default connect(() => {}, mapDispatchToProps)(BucketRow);