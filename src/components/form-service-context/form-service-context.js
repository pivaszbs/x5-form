import React from 'react';

const {
  Provider: FormServiceProvider,
  Consumer: FormServiceConsumer
} = React.createContext();

export {
  FormServiceProvider,
  FormServiceConsumer
};