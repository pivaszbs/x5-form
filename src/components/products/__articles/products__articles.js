import React from 'react'
import { Column } from '../../ui';
import ProductsRow from '../__row/products__row';

function ProductsArticles({ items = [], pick }) {
    const articles = items.map(({ id, artNo, active }) => (
        <ProductsRow
            key={id}
            active={active}
            className='text__article'
            onClick={() => pick(id)}
        >
            { artNo }
        </ProductsRow>
    ));

    return (
        <Column className='products__articles'>
            {articles}
        </Column>
    )
}

export default ProductsArticles;