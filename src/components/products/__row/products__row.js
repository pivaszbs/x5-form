import React from 'react'
import { Row, Text } from '../../ui';
import clsx from 'clsx';

function ProductsRow({ active, children, className, ...others }) {
    return (
        <Row className={clsx('products__row', active && 'row_active')} {...others}>
            <Text variant='body' className={className}>{ children }</Text>
        </Row>
    )
}

export default ProductsRow
