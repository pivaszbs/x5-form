import React from 'react'
import ProductsArticles from '../__articles/products__articles';
import ProductsNames from '../__names/products__names';
import { Row } from '../../ui';

function ProductsContent({ items, pick }) {
    return (
        <Row className='products__content'>
            <ProductsArticles items={items} pick={pick} />
            <ProductsNames items={items} pick={pick} />
        </Row>
    )
}

export default ProductsContent
