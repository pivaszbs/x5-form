import React, { Component } from 'react'
import './products.sass';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { withFormService } from '../hoc';

import { productAddToBucket, fetchProducts } from '../../model/actions';
import ProductsContent from './__content/products__content';
import ProductsTitles from './__titles/products__titles';

class Products extends Component {
    componentDidMount() {
        this.props.fetchProducts();
    }

    render() {
        const { items, pick } = this.props;

        return (
            <div className='products'>
                <ProductsTitles />
                <ProductsContent items={items} pick={pick} />
            </div>
        )
    }
}

const mapStateToProps = ({ product }) => {
    return {
        items: product
    }
};


const mapDispatchToProps = (dispatch, { formService }) => {
    return {
        pick: (id) => dispatch(productAddToBucket(id)),
        fetchProducts: fetchProducts(formService, dispatch)
    }
}

export default compose(
    withFormService(),
    connect(mapStateToProps, mapDispatchToProps)
)(Products)
