import React from 'react'
import { Row, Text } from '../../ui';

function ProductsTitles() {
    return (
        <Row>
            <Text className='products__title products__title_art' variant='h2'>
                Артикул
            </Text>
            <Text className='products__title products__title_name' variant='h2'>
                Наименование
            </Text>
        </Row>
    )
}

export default ProductsTitles
