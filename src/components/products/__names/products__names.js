import React from 'react'

import { Column } from '../../ui';

import ProductsRow from '../__row/products__row';

function ProductsNames({ items = [], pick }) {
    const names = items.map(({ active, name, id }) => (
        <ProductsRow
            key={id}
            active={active}
            className='text__name'
            onClick={() => pick(id)}
        >
            { name }
        </ProductsRow>
    ));

    return (
        <Column className='products__names'>
            {names}
        </Column>
    )
}

export default ProductsNames;